*** Settings ***
Library     RPA.Excel.Files

*** Test Cases ***
TC1
    Open Workbook    path=C:${/}Automation Concepts${/}orange_data.xlsx
    @{sheet_names}   List Worksheets
    Log To Console    ${sheet_names}

    FOR    ${sheet_name}    IN    @{sheet_names}
        Log To Console    ${sheet_name}
    END

TC2
    Open Workbook    path=C:${/}Automation Concepts${/}orange_data.xlsx
    ${sheet}    Read Worksheet      VerifyInvalidLogin
    Log To Console    ${sheet}
    Log To Console    ${sheet}[0]
    Log To Console    ${sheet}[0][A]
    Log To Console    ${sheet}[0][B]
    Log To Console    ${sheet}[2][B]

    ${row_count}    Get Length    ${sheet}
    Log To Console    ${row_count}
    ${column_count}    Get Length    ${sheet}[0]
    Log To Console    ${column_count}

TC3
    Open Workbook    path=C:${/}Automation Concepts${/}orange_data.xlsx
    ${sheet}    Read Worksheet      Sheet4
    Set Worksheet Value    1    1    hello
    Save Workbook
    ${value}    Get Worksheet Value    1    1
    Log To Console    ${value}
    Close Workbook
