*** Settings ***
Library     RPA.Word.Application

*** Test Cases ***
TC1 New Document
    Open Application
    Create New Document
    Write Text    text=hello, how are you
    Save Document As    filename=C:${/}Automation Concepts${/}data.docx
    Quit Application

TC2 Open Existing Document
    Open Application
    Open File   filename=C:${/}Automation Concepts${/}data.docx
    ${texts}     Get All Texts
    Log To Console    ${texts}
