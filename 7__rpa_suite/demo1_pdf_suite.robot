*** Settings ***
Library     RPA.PDF
Library     Collections

*** Test Cases ***
TC1
    ${res}      Get Text From Pdf       source_path=C:${/}Automation Concepts${/}orange_data.pdf
    Log   ${res}
    ${page}     Convert To Integer    1
    Log    ${res}[${page}]

    ${keys}     Get Dictionary Keys    ${res}
    Log    ${keys}

    FOR    ${key}    IN    @{keys}
        Log    ${res}[${key}]
    END

TC2
    Open Pdf    source_path=C:${/}Automation Concepts${/}orange_data.pdf
    ${res}      Get Text From Pdf
    Log   ${res}
    ${num}  Get Number Of Pages
    Log    ${num}
