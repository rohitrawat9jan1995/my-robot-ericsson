*** Settings ***
Library     DatabaseLibrary

Suite Setup     Connect To Database     dbapiModuleName=pymysql     dbName=dbfree_db
    ...  dbUsername=dbfree_db   dbPassword=12345678     dbHost=db4free.net     dbPort=3306

Suite Teardown      Disconnect From Database

*** Test Cases ***
TC1
    ${row_count}     Row Count    select * from Products
    Log To Console    ${row_count}

TC2
    Row Count Is Equal To X    select * from Products    153
    Row Count Is Greater Than X    select * from Products    143
    Row Count Is Less Than X    select * from Products    157
    Row Count Is 0    select * from Products where product_id=213214

TC7
    Execute Sql String    Insert into Products (product_id, ProductName, description) values ('20003','aa','bb')
    Row Count Is Equal To X    select * from Product where product_id=20003    1