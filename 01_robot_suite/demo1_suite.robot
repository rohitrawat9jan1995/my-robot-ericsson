*** Settings ***
Library     DateTime

*** Test Cases ***
TC1
    Log To Console    Rohit

TC2
    Log To Console    Rohit2
    Log To Console    message=rohit2.2

TC3
    ${my_name}  Set Variable    rohit3
    Log To Console    ${my_name}
    Log    ${my_name}
    
    
TC4
    ${radius}   Set Variable    10
    #write logic to alculate area of circle
    ${result}    Evaluate    expression=3.14*${radius}*${radius}
    Log To Console    ${result}
    
TC5
    ${currentdate}      Get Current Date
    Log To Console    ${currentdate}