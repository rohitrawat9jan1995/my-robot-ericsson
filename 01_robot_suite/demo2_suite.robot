*** Settings ***
Library    String
*** Test Cases ***
TC1
    ${name}     Set Variable    robot section
    #convert to uppercase and print
    ${upper_case}   Convert To Upper Case   ${name}
    Log To Console    ${upper_case}
    
TC2
    ${num1}      Set Variable    102
    ${num2}      Set Variable    201
    #sum above two values and print
    ${result}   Evaluate    ${num1}+${num2}
    Log To Console    ${result}

TC3
    ${num1}      Set Variable    $102,000
    ${num2}      Set Variable    $201,121,000
    #sum above two values and print
    ${result}   Evaluate    ${num1}+${num2}
    Log To Console    ${result}
    
TC4
    ${num1}      Set Variable    $102,000
    ${num1}     Remove String    ${num1}    $   ,
    ${num2}      Set Variable    $201,121,000
    ${num2}     Remove String    ${num2}    $   ,
    #sum above two values and print
    ${result}   Evaluate    ${num1}+${num2}
    Log To Console    ${result}