*** Settings ***
Library     OperatingSystem

*** Test Cases ***
TC1
    Log To Console    C:\\Automation Concepts\\rohit
    Log To Console    C:${/}Automation Concepts${/}rohit

TC2
    Create Directory   path=C:${/}Automation Concepts${/}rohit
    Directory Should Exist    path=C:${/}Automation Concepts${/}rohit
    Remove Directory    path=C:${/}Automation Concepts${/}rohit
    Directory Should Not Exist    path=C:${/}Automation Concepts${/}rohit

TC3
    List Directories In Directory    path=C:${/}Program Files${/}

TC4
    #list all the files in directory
    List Files In Directory    path=C:${/}Program Files${/}
    
TC5
    Log To Console    ${CURDIR}
    Log To Console    ${OUTPUT_DIR}
    Log To Console    ${EXECDIR}
    Log To Console    Environment${SPACE}${SPACE}variable
    Log To Console    ${TEMPDIR}
    Log To Console    ${SUITE_NAME}
    Log To Console    ${TEST_NAME}


TC6
