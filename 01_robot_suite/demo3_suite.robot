*** Settings ***
Library     DateTime
Library    Collections

*** Variables ***
${BROWSER_NAME}     Chrome
@{COLOURS}      red,    green,  yellow
&{my_details}   name=rohit      role=learner      mobile=9999
*** Test Cases ***
TC1
    Log To Console    ${BROWSER_NAME}

TC2
    Log To Console    ${BROWSER_NAME}
    Log To Console    ${COLOURS}
    Log To Console    ${COLOURS}[1]
    ${size}     Get Length    ${COLOURS}
    Log To Console    ${size}


TC3
    @{fruits}   Create List     Mango   apple   orange
    Log To Console    ${fruits}

    #print the size of the list
    ${size}     Get Length    ${fruits}
    Log To Console    ${size}

    #append grapes to the list
    Append To List    ${fruits}     grapes
    Log To Console    ${fruits}

    #remove apple from the list
    Remove Values From List    ${fruits}    apple
    Log To Console    ${fruits}

    #insert jackfruit at the index 0


    #print the list
    Log To Console    ${fruits}


TC4
