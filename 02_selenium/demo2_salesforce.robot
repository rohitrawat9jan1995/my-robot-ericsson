*** Settings ***
Library     SeleniumLibrary
Documentation   Having all SignUp TCs

*** Test Cases ***
TC1 SignUp
    Open Browser    url=https://www.salesforce.com/in/form/sem/salesforce-crm/?d=7013y000002hbRLAAY&nc=7013y000002RHs6AAG&utm_source=google&utm_medium=sem&utm_campaign=in_br_alllobcon&utm_content=exact_7013y000002k1z5aakbr-google-/form/sem/salesforce-crm/&ef_id=EAIaIQobChMI2P3NhM-K_wIVCXZgCh2u1AVlEAAYASAAEgJoR_D_BwE:G:s&s_kwcid=AL!4720!3!592749491004!e!!g!!salesforce&&ev_sid=3&ev_ln=salesforce&ev_lx=kwd-94920873&ev_crx=592749491004&ev_mt=e&ev_n=g&ev_ltx=&ev_pl=&ev_pos=&ev_dvc=c&ev_dvm=&ev_phy=9062112&ev_loc=&ev_cx=16895847353&ev_ax=137218384684&ev_efid=EAIaIQobChMI2P3NhM-K_wIVCXZgCh2u1AVlEAAYASAAEgJoR_D_BwE:G:s&url=!https://clickserve.dartsearch.net/link/click?lid=43700074078535017&ds_s_kwgid=58700008151233901&gclid=EAIaIQobChMI2P3NhM-K_wIVCXZgCh2u1AVlEAAYASAAEgJoR_D_BwE&gclsrc=aw.ds      browser=Chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    30s
#    Click Element    link=Create new account
#    Input Text    name=firstname    john
    Input Text    name=UserFirstName    john
    Input Text    name=UserLastName    wik
    Select From List By Index     name=UserTitle    7
    Input Text    name=UserEmail    johnwick@gmail.com
    Input Text    name=CompanyName    salesforce
    Select From List By Index     name=CompanyEmployees    1
    Click Element   xpath=//button[contains(text(),'Watch it in action')]
    Sleep    30s
    Get Text    xpath=//span[contains(text(),'Enter a valid phone number')]
    Element Text Should Be   xpath=//span[contains(text(),'Enter a valid phone number')]      Enter a valid phone number
