*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://datatables.net/extensions/select/examples/initialisation/checkbox.html

TC2 Get name from page one
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://datatables.net/extensions/select/examples/initialisation/checkbox.html
    FOR    ${counter}    IN RANGE    1    11
        ${name}     Get Text    xpath=//table[@id="example"]/tbody/tr[${counter}]/td[2]
        Log To Console      ${name}
        IF    $name == 'Bradley Greer'
            Click Element    xpath=//table[@id="example"]/tbody/tr[${counter}]/td[1]
            Exit For Loop
        END
    END


TC3 Get the name from all the pages
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://datatables.net/extensions/select/examples/initialisation/checkbox.html
    ${number_of_pages}       Get Element Count    //a[@class="paginate_button "]
    FOR    ${i}    IN RANGE    1    ${number_of_pages}+2
        IF    ${i} == 1
            Sleep    1s
        ELSE
            Click Element    //a[@class="paginate_button " and contains(text(),'${i}')]
            Sleep    3s
        END
        ${number_of_data}       Get Element Count    //table[@id="example"]/tbody/tr
        FOR    ${counter}    IN RANGE    1    ${number_of_data}+1
            ${name}     Get Text    xpath=//table[@id="example"]/tbody/tr[${counter}]/td[2]
            Log To Console      ${name}
        END
    END