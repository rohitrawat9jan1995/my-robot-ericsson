*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://www.ilovepdf.com/pdf_to_word
    Choose File    xpath=//input[@type='file']    C:${/}Automation Concepts${/}selenium_tutorial.pdf


TC2 Nasscom
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://nasscom.in/about-us/contact-us
    Mouse Over    link=Membership
    Mouse Over    link=Become a Member
    Click Element    link=Membership Benefits


TC4 Php Javascript
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://phptravels.net/
    Execute Javascript      document.querySelector('#checkin').value='22/05/2023'
    Execute Javascript      document.querySelector('#checkout').value='23/05/2023'
    Sleep    5s
    Execute Javascript      document.querySelector("input[name='checkout']").value='24/05/2023'
    Sleep    5s


TC5 Php Javascript2
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://phptravels.net/
    ${ele}      Get WebElement    xpath=//input[@name='checkout']
