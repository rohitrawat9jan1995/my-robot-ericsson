*** Settings ***
Library     SeleniumLibrary

*** Keywords ***
Launch Browser
    [Arguments]     ${browsername}      ${url}
    Open Browser    browser=${browsername}
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=http://demo.openemr.io/b/openemr/
    Go To    url=${url}


*** Test Cases ***
TC1
    Launch Browser    edge    http://demo.openemr.io/b/openemr/
    Input Text    css=#authUser    admin
    Input Password    css=#clearPass    pass
    Select From List By Label    css=select[name="languageChoice"]    English (Indian)
    Click Element    css=#login-button
    Click Element    xpath=//div[text()='Patient']
    Click Element    xpath=//div[text()='New/Search']
    
    Select Frame    xpath=//iframe[@name="pat"]
    Input Text    id=form_fname    rohit
    Input Text    id=form_lname    rawat
    Input Text    id=form_DOB       2023-05-24
    Select From List By Label    css=select[id="form_sex"]      Male
    Click Element    id=create
#    Run Keyword And Ignore Error    Click Element    class=newclose
    
    Select Frame    xpath=//iframe[@id="modalframe"]
    Click Element    xpath=//button[text()="Confirm Create New Patient"]
    Unselect Frame

    ${actual_alert_text}    Handle Alert    action=ACCEPT       timeout=20s
    Run Keyword And Ignore Error    Click Element    xpath=//div[@class="closeDlgIframe"]
    
    Select Frame    xpath=//iframe[@name="pat"]
    Element Should Contain    xpath=//span[contains(text(),"Medical Record Dashboard")]    Medical Record Dashboard
    Unselect Frame
    Close Browser

    