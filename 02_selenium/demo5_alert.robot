
*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://netbanking.hdfcbank.com/netbanking/IpinResetUsingOTP.htm
    Click Element    xpath=//img[@src="gif/go.gif"]
    ${alert_message}    Handle Alert    ACCEPT
    Log    ${alert_message}
    Should Be Equal    ${alert_message}    Customer ID${space}${space}cannot be left blank.
    Should Be Equal As Strings    ${alert_message}    Customer ID${space} cannot be left blank.
