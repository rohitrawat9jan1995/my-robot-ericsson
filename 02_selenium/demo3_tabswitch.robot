*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    url=https://www.db4free.net/    browser=Chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
#    Click Element   xpath=//b[ns(text(),'phpMyAdmin')]
    Click Element    partial link=phpMyAdmin
#    Sleep    3s
    Switch Window   db4free.net - MySQL Database for free
#    Sleep    3s
    Switch Window   phpMyAdmin
    Input Text    id=input_username    rohit
    Input Text    id=input_password    welcome123
    Click Element    xpath=//input[@value="Log in"]
#    Sleep    2s
    Element Should Contain    id=pma_errors    Access denied for user
    Page Should Contain    Access denied for user


TC2
#1.  Navigate onto https://www.online.citibank.co.in/
    Open Browser    url=https://www.online.citibank.co.in/    browser=Chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
#2.  Close if any pop up comes

    Run Keyword And Ignore Error    Click Element    class=newclose
    Run Keyword And Ignore Error    Click Element    class=newclose2
    Run Keyword And Ignore Error    Click Element    class=newclose3
#3.  Click on Login
    Click Element    xpath=//a[@id="loginId"]/img[@title="LOGIN NOW"]
#4.  Click on Forgot User ID?
    Switch Window   Citibank India
#    Click Element    xpath=//div[@onclick="ForgotUserID();"]
    Click Element    xpath=//div[contains(@onclick,'ForgotUserID')]
#5.  Choose Credit Card
    Click Element    link=select your product type
    Click Element    link=Credit Card


#6.  Enter credit card number as 4545 5656 8887 9998
    Input Text    id=citiCard1    4545
    Input Text    css=#citiCard2    5656
    Input Text    id=citiCard3    8887
    Input Text    id=citiCard4    9998

#7.  Enter cvv number
    Input Text    id=cvvnumber    123

#8.  Enter date as “14/04/2022”
    Click Element    name=DOB
    Select From List By Value     class=ui-datepicker-year    1995
    Select From List By Value     class=ui-datepicker-month   0
    Click Element    link=9

#9.  Click on Proceed
    Click Element    xpath=//input[@value="PROCEED"]

#10.  Get the text and print it “Please accept Terms and Conditions”
#    Element Should Contain    xpath=//div[@role="dialog"]//div[@id="ui-id-9"]    • Please accept Terms and Conditions
    Element Should Contain    xpath=//li[contains(text(), 'accept Terms')]   • Please accept Terms and Conditions
