
*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://netbanking.hdfcbank.com/netbanking/
    Select Frame    xpath=//frame[@name="login_page"]
    Input Text    xpath=//div[@class="inputfield ibvt loginData"]/input[@name="fldLoginUserId"]    text123
    Element Should Contain    id=hdfcwelcmetxtId     Welcome to HDFC Bank NetBanking
    Click Element    link=CONTINUE
    Sleep    3s
    Unselect Frame
