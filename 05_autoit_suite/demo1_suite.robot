*** Settings ***
Library     SeleniumLibrary
Library     AutoItLibrary

Suite Teardown      Sleep    2s     AND     Close Browser

*** Test Cases ***
TC1 Windows Credential Using URL
    Open Browser  browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    10s
    #https://username:password@URL
    Go To    url=https://admin:admin@the-internet.herokuapp.com/basic_auth
    Sleep    5s
    [Teardown]  Close Browser

TC2 Windows Credential Using AutoIT
    Open Browser  browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    10s
    Go To    url=https://@the-internet.herokuapp.com/basic_auth
    Sleep    2s
    AutoItLibrary.Send      admin
    AutoItLibrary.Send      {TAB}
    AutoItLibrary.Send      admin
    AutoItLibrary.Send      {ENTER}
    Sleep    5s
    [Teardown]  Close Browser

TC3
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://www.ilovepdf.com/pdf_to_word
    Click Element    id=pickfiles
    Sleep    5s
    Control Focus   open    ${EMPTY}    Edit1
    Control Set Text    open    ${EMPTY}        C:${/}Automation Concepts${/}selenium_tutorial.pdf
    Control Click   open    ${EMPTY}    Button1