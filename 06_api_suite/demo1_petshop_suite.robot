*** Settings ***
Library     RequestsLibrary


*** Test Cases ***
TC1 Find Valid Pet By Id
    Create Session    alias=petshop    url=https://petstore.swagger.io/v2
    ${response}     GET On Session      alias=petshop   url=pet/5       expected_status=200
    Log    ${response}
    Log    ${response.json()}
    Log    ${response.status_code}
    Status Should Be    200
    Log    ${response.json()}[id]
    Should Be Equal As Integers    ${response.json()}[id]    5

TC2 FInd valid status
    Create Session    petshop    url=https://petstore.swagger.io/v2
    ${response}     GET On Session      alias=petshop   url=pet/findByStatus?status=sold       expected_status=200
    Log    ${response}
    Log    ${response.json()}
    Log    ${response.status_code}
    Status Should Be    200
    Log    ${response.json()}[0]
    Log    ${response.json()}[0][status]
    Should Be Equal As Strings    ${response.json()}[0][status]    sold
    ${size}     Get Length    ${response.json()}
    FOR    ${i}    IN RANGE    0    ${size}
        Log    ${response.json()}[${i}][status]
        Should Be Equal As Strings    ${response.json()}[${i}][status]    sold
    END

TC3 Find invalid pet by ID
    Create Session    alias=petshop    url=https://petstore.swagger.io/v2
    ${response}     GET On Session      alias=petshop   url=pet/522       expected_status=404
    Log    ${response}
    Log    ${response.json()}
    Log    ${response.status_code}
