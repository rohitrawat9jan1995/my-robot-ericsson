*** Settings ***
Library     RequestsLibrary
Library    OperatingSystem

Suite Setup     Create Session    alias=petshop    url=https://petstore.swagger.io/v2
Suite Teardown      Delete All Sessions


*** Keywords ***
Get Token
    Create Session    alias=abc    url=https://petstore.swagger.io/v2
    ${response}     GET On Session      alias=abc   url=pet/5       expected_status=200
    [Return]    ${response.json()}[id]


*** Test Cases ***
TC1 Add Valid Pet


    &{header_dic}    Create Dictionary       Content-Type=application/json      Connection=keep-alive
    ${json}     Get Binary File     ${EXECDIR}${/}test_data${/}new_pet.json

    ${response}     POST On Session     alias=petshop   url=pet   headers=${header_dic}     data=${json}    expected_status=200
    Should Be Equal As Integers    ${response.json()}[id]    6501
    Should Be Equal As Strings    ${response.json()}[name]    doggie-6501

TC2 Update Valid Pet
    Create Session    alias=petshop    url=https://petstore.swagger.io/v2

    &{header_dic}    Create Dictionary       Content-Type=application/json      Connection=keep-alive
    ${json}     Get Binary File     ${EXECDIR}${/}test_data${/}update_pet.json

    ${response}     PUT On Session     alias=petshop   url=pet   headers=${header_dic}     data=${json}    expected_status=200
    Should Be Equal As Integers    ${response.json()}[id]    6501
    Should Be Equal As Strings    ${response.json()}[name]    doggie-6501-update

TC3 Delete Valid Pet By Id
    Create Session    alias=petshop    url=https://petstore.swagger.io/v2
    &{header_dic}    Create Dictionary       api_key=217923ussd7sd
    ${response}     DELETE On Session       alias=petshop       url=pet/6501        headers=${header_dic}       expected_status=200
    Log     ${response}


TC4 Delete Invalid Pet By Id
    Create Session    alias=petshop    url=https://petstore.swagger.io/v2
    ${token}    Get Token
    &{header_dic}    Create Dictionary       api_key=${token}
    ${response}     DELETE On Session       alias=petshop       url=pet/6501        headers=${header_dic}       expected_status=404
