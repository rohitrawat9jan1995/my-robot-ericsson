*** Settings ***
Library     RequestsLibrary

Test Template   Find Valid Pet By Id Templete

*** Test Cases ***
TC1
    5
TC2
    6
TC3
    8

*** Keywords ***
Find Valid Pet By Id Templete
    [Arguments]     ${penId}
    Create Session    alias=petshop    url=https://petstore.swagger.io/v2
    ${response}     GET On Session      alias=petshop   url=pet/${penId}       expected_status=200
    Log    ${response}
    Log    ${response.json()}
    Log    ${response.status_code}
    Status Should Be    200
    Log    ${response.json()}[id]
    Should Be Equal As Integers    ${response.json()}[id]    ${penId}
    Log    ${response.json()}[name]